vim-slash-lite
==============

vim-slash-lite provides a set of mappings for enhancing in-buffer search 
experience in [neo]Vim. 
It's essentially a cut-down version of [junegunn's 
vim-slash](https://github.com/junegunn/vim-slash) and can be considered its 
strict subset.

It does away with automatic search highlight clearing, and only provides 
improved star-search (visual-mode, highlighting without moving).

I recommend just mapping your Esc to `:noh`, as follows.

```vim
nnoremap <silent> <Esc> :noh<CR><Esc>
```

I've seen reports it messes up Esc-encoded keys in the terminal, but this 
really doesn't apply for `nvim`.

Installation
------------

Using [vim-plug](https://gitlab.com/andrejr/vim-slash-lite):

```vim
Plug 'https://gitlab.com/andrejr/vim-slash-lite.git'
```

Customization
-------------

#### Avoid remapping `gd` and `gD`

There is only one setting provided for vim-slash-lite:
`g:slash_lite_avoid_mapping_gd`.
It's `0` by default, and lets you avoid mapping `gd` and `gD`, which is useful 
if you've remapped that binding to something else, like `coc.nvim`.

#### `zz` after search

Places the current match at the center of the window.

```vim
noremap <plug>(slash-after) zz
```


Credits
-------

[Junegunn Choi](https://github.com/junegunn), the original author whose
creation I've [cut up](https://www.youtube.com/watch?v=3sIYe74sczE).
